#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PySide import QtGui

from src.filters.adaptive_measure import AdaptiveMeasure
from src.filters.adaptive_model import AdaptiveModel
from src.filters.filter import Kalman, Kalman_Q
from src.filters.inverted import Inverted
#from src.filters.model import ModelJumped as Model
from src.filters.model import ObjectModel as Model
import src.filters.plotter as plt

__version__ = '0.0.1'


class MainWindow(QtGui.QWidget):
    NOIZE_CONSTS = {
        'loc': 0,
        'scale_V': 1e-4,  # 1e-4 .. 6,
        'scale_W': 1e-8,
    }

    def __init__(self):
        super(MainWindow, self).__init__()

        self.initUI()

    def initUI(self):

        scale_V = QtGui.QLabel('scale V')
        scale_W = QtGui.QLabel('scale W')
        #loc = QtGui.QLabel('mean')
        kalm_R = QtGui.QLabel('kalman R')
        kalm_Q = QtGui.QLabel('kalman Q')

        self.scale_VEdit = QtGui.QLineEdit(str(self.NOIZE_CONSTS['scale_V']))
        self.scale_WEdit = QtGui.QLineEdit(str(self.NOIZE_CONSTS['scale_W']))
        self.locEdit = QtGui.QLineEdit(str(self.NOIZE_CONSTS['loc']))

        self.kalm_R_Edit = QtGui.QLineEdit()
        self.kalm_Q_Edit = QtGui.QLineEdit()

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)

        grid.addWidget(scale_V, 1, 0)
        grid.addWidget(self.scale_VEdit, 1, 1)

        grid.addWidget(scale_W, 2, 0)
        grid.addWidget(self.scale_WEdit, 2, 1)

        #grid.addWidget(loc, 3, 0)
        #grid.addWidget(self.locEdit, 3, 1)

        grid.addWidget(kalm_R, 3, 0)
        grid.addWidget(self.kalm_R_Edit, 3, 1)

        grid.addWidget(kalm_Q, 4, 0)
        grid.addWidget(self.kalm_Q_Edit, 4, 1)

        button_measure = QtGui.QPushButton(u"Измерения", self)
        button_measure.clicked.connect(self.button_measure_click)
        grid.addWidget(button_measure, 5, 0, 5, 0)

        button_kalman = QtGui.QPushButton(u"Фильтр Калмана")
        button_kalman.clicked.connect(self.button_kalman_click)
        grid.addWidget(button_kalman, 6, 0, 6, 0)

        button_adaptive_measures = QtGui.QPushButton(u"Адаптивный по измерениям")
        button_adaptive_measures.clicked.connect(self.button_adaptive_measures_click)
        grid.addWidget(button_adaptive_measures, 7, 0, 7, 0)

        button_adaptive_model = QtGui.QPushButton(u"Адаптивный по модели")
        button_adaptive_model.clicked.connect(self.button_adaptive_model_click)
        grid.addWidget(button_adaptive_model, 8, 0, 8, 0)

        button_inversed_filter = QtGui.QPushButton(u"Обратный")
        button_inversed_filter.clicked.connect(self.button_inversed_filter_click)
        grid.addWidget(button_inversed_filter, 9, 0, 9, 0)

        self.setLayout(grid)

        self.setGeometry(300, 300, 300, 350)
        self.setWindowTitle('Filters')
        self.show()

    def _get_noise(self):
        return {
            'loc': float(self.locEdit.text()),
            'scale_V': float(self.scale_VEdit.text()),  # 1e-4 .. 6,
            'scale_W': float(self.scale_WEdit.text()),
        }

    def button_measure_click(self):
        model = Model(noise=self._get_noise())
        for k in xrange(5000):
            model.measure()
        plt.plot_channel_data(model.measures, model.ideals, title=u"Моделирование измерений")

    def button_kalman_click(self):
        kalm = Kalman_Q(noise=self._get_noise())
        if self.kalm_Q_Edit.text() and self.kalm_Q_Edit.text():
            kalm.set_Q(float(self.kalm_Q_Edit.text()))
            kalm.set_R(float(self.kalm_R_Edit.text()))
        for k in xrange(5000):
            kalm.prediction()
        plt.plot_channel_data(kalm.predictions, kalm.ideals, title=u"Фильтр Калмана")

    def button_adaptive_measures_click(self):
        kalm = AdaptiveMeasure(noise=self._get_noise())
        for k in xrange(5000):
            kalm.prediction()
        plt.plot_channel_data(kalm.predictions, kalm.ideals, title=u"Адаптивный по измерениям")

    def button_adaptive_model_click(self):
        kalm = AdaptiveModel(noise=self._get_noise())
        for k in xrange(5000):
            kalm.prediction()
        plt.plot_channel_data(kalm.predictions, kalm.ideals, title=u"Адаптивный по модели")

    def button_inversed_filter_click(self):
        inv = Inverted(noise=self._get_noise())
        for k in xrange(5000):
            inv()
        inv.estimate()
        plt.plot_channel_triple(inv.predictions, inv.estimates, inv.ideals, title=u"Обратный фильтр")


def main():
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
