#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PySide import QtGui

from matplotlib import use, rcParams
use('Qt4Agg')
rcParams['backend.qt4']='PySide'

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from src.filters.adaptive_measure import AdaptiveMeasure
from src.filters.adaptive_model import AdaptiveModel
from src.filters.filter import Kalman, Kalman_Q
from src.filters.inverted import Inverted
from src.filters.model import ObjectModel


class MainWindow(QtGui.QWidget):
    NOIZE_CONSTS = {
        'loc': 0,
        'scale_V': 1e-4,  # 1e-4 .. 6,
        'scale_W': 1e-8,
    }

    def _get_noise(self):
        return {
            'loc': float(self.locEdit.text()),
            'scale_V': float(self.scale_VEdit.text()),  # 1e-4 .. 6,
            'scale_W': float(self.scale_WEdit.text()),
        }

    def __init__(self):
        super(MainWindow, self).__init__()

        vlayout = self.initUi()
        self.hlayout = QtGui.QHBoxLayout()
        self.hlayout.addLayout(vlayout)
        self.setLayout(self.hlayout)

        self.fig = Figure(figsize=(600, 600), dpi=72, edgecolor=(0,0,0))
        self.ax = self.fig.add_subplot(311)
        self.ay = self.fig.add_subplot(312)
        self.az = self.fig.add_subplot(313)

        self.canvas = FigureCanvas(self.fig)
        self.hlayout.addWidget(self.canvas)

        self.show()

    def initUi(self):
        scale_V = QtGui.QLabel('scale V')
        scale_W = QtGui.QLabel('scale W')
        #loc = QtGui.QLabel('mean')
        kalm_R = QtGui.QLabel('kalman R')
        kalm_Q = QtGui.QLabel('kalman Q')

        self.scale_VEdit = QtGui.QLineEdit(str(self.NOIZE_CONSTS['scale_V']))
        self.scale_WEdit = QtGui.QLineEdit(str(self.NOIZE_CONSTS['scale_W']))
        self.locEdit = QtGui.QLineEdit(str(self.NOIZE_CONSTS['loc']))

        self.kalm_R_Edit = QtGui.QLineEdit()
        self.kalm_Q_Edit = QtGui.QLineEdit()

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)

        grid.addWidget(scale_V, 1, 0)
        grid.addWidget(self.scale_VEdit, 1, 1)

        grid.addWidget(scale_W, 2, 0)
        grid.addWidget(self.scale_WEdit, 2, 1)

        #grid.addWidget(loc, 3, 0)
        #grid.addWidget(self.locEdit, 3, 1)

        grid.addWidget(kalm_R, 3, 0)
        grid.addWidget(self.kalm_R_Edit, 3, 1)

        grid.addWidget(kalm_Q, 4, 0)
        grid.addWidget(self.kalm_Q_Edit, 4, 1)

        button_measure = QtGui.QPushButton(u"Измерения", self)
        button_measure.clicked.connect(self.button_measure_click)
        grid.addWidget(button_measure, 6, 0, 6, 0)

        button_kalman = QtGui.QPushButton(u"Фильтр Калмана")
        button_kalman.clicked.connect(self.button_kalman_click)
        grid.addWidget(button_kalman, 7, 0, 7, 0)

        button_adaptive_measures = QtGui.QPushButton(u"Адаптивный по измерениям")
        button_adaptive_measures.clicked.connect(self.button_adaptive_measures_click)
        grid.addWidget(button_adaptive_measures, 8, 0, 8, 0)

        button_adaptive_model = QtGui.QPushButton(u"Адаптивный по модели")
        button_adaptive_model.clicked.connect(self.button_adaptive_model_click)
        grid.addWidget(button_adaptive_model, 9, 0, 9, 0)

        button_inversed_filter = QtGui.QPushButton(u"Обратный")
        button_inversed_filter.clicked.connect(self.button_inversed_filter_click)
        grid.addWidget(button_inversed_filter, 10, 0, 10, 0)

        return grid

    def updateUi(self, plot_data, plot_raw_data, title=None, plot_extra_data=None):
        self.hlayout.removeWidget(self.canvas)
        self.ax.cla()
        self.ay.cla()
        self.az.cla()
        a = [i for i in range(plot_raw_data.shape[0])]

        if plot_extra_data is None:
            self.ax.plot(a, plot_data.T[0], 'r',
                            a, plot_raw_data.T[0], 'b--',)
            self.ay.plot(a, plot_data.T[1], 'r',
                            a, plot_raw_data.T[1], 'b--',)
            self.az.plot(a, plot_data.T[2], 'r',
                            a, plot_raw_data.T[2], 'b--',)
        else:
            self.ax.plot(a, plot_data.T[0], 'r',
                 a, plot_raw_data.T[0], 'b--',
                 a, plot_extra_data.T[0], 'g--',)
            self.ay.plot(a, plot_data.T[1], 'r',
                 a, plot_raw_data.T[1], 'b--',
                 a, plot_extra_data.T[1], 'g--',)
            self.az.plot(a, plot_data.T[2], 'r',
                 a, plot_raw_data.T[2], 'b--',
                 a, plot_extra_data.T[2], 'g--',)

        self.canvas = FigureCanvas(self.fig)
        self.hlayout.addWidget(self.canvas)

        if title:
            self.setWindowTitle(title)

    def draw(self, plot_data, plot_raw_data, title=None, plot_extra_data=None):
        self.updateUi(plot_data, plot_raw_data, title, plot_extra_data)

    def button_measure_click(self):
        model = ObjectModel(noise=self._get_noise())
        for k in xrange(5000):
            model.measure()
        self.draw(model.measures, model.ideals, title=u"Моделирование измерений")

    def button_kalman_click(self):
        kalm = Kalman_Q(noise=self._get_noise())
        if self.kalm_Q_Edit.text() and self.kalm_Q_Edit.text():
            kalm.set_Q(float(self.kalm_Q_Edit.text()))
            kalm.set_R(float(self.kalm_R_Edit.text()))
        for k in xrange(5000):
            kalm.prediction()
        self.draw(kalm.predictions, kalm.ideals, title=u"Фильтр Калмана")

    def button_adaptive_measures_click(self):
        kalm = AdaptiveMeasure(noise=self._get_noise())
        for k in xrange(5000):
            kalm.prediction()
        self.draw(kalm.predictions, kalm.ideals, title=u"Адаптивный по измерениям")

    def button_adaptive_model_click(self):
        kalm = AdaptiveModel(noise=self._get_noise())
        for k in xrange(5000):
            kalm.prediction()
        self.draw(kalm.predictions, kalm.ideals, title=u"Адаптивный по модели")

    def button_inversed_filter_click(self):
        inv = Inverted(noise=self._get_noise())
        for k in xrange(5000):
            inv()
        inv.estimate()
        self.draw(inv.predictions, inv.estimates, title=u"Обратный фильтр", plot_extra_data=inv.ideals)


def main():
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
