#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from scipy import stats as ss
import math
from src.filters import plotter as plt
from src.filters.filter import Kalman


class Tubed_filter(Kalman):
    # http://sky2high.net/tag/numpy/
    # http://sky2high.net/2012/08/%D1%80%D0%B0%D0%B7%D0%B1%D0%B5%D1%80%D0%B5%D0%BC%D1%81%D1%8F-%D1%81-%D0%B8%D0%B7%D0%BC%D0%B5%D1%80%D0%B5%D0%BD%D0%B8%D0%B5%D0%BC-%D1%81%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D1%8B%D1%85-%D0%B2%D0%B5-4/
    PROBABILITY = {
        'n': 5,
        'alpha': 0.05,  # 2 sigma
        'tail_size': 100
    }

    def __init__(self, *args, **kwargs):
        super(Tubed_filter, self).__init__(*args, **kwargs)

        self.tubed_predictions = np.array(((),))

    def set_tubed_predictions(self, tubed_pr):
        if self.tubed_predictions.size == 0:
            self.tubed_predictions = np.append(self.tubed_predictions, [tubed_pr], axis=1)
            return

        self.tubed_predictions = np.insert(self.tubed_predictions, 0, tubed_pr, axis=0)

    def limits_simple(self, axis):
        #http://adventuresinpython.blogspot.ru/2012/12/confidence-intervals-in-python.html
        sample = self.get_sample(axis)
        n, min_max, mean, var, skew, kurt = ss.describe(sample)
        std = math.sqrt(var)

        return ss.norm.interval((1 - self.PROBABILITY['alpha']), loc=mean, scale=std/math.sqrt(n))

    def get_sample(self, axis):
        return self.predictions[-self.PROBABILITY['tail_size']:].T[axis]

    def inverted_mean(self, axe):
        return np.divide(1., np.mean(self.get_sample(axe)))

    def limits(self, axe):
        #http://stackoverflow.com/questions/14813530/poisson-confidence-interval-with-numpy
        n = self.PROBABILITY['tail_size']
        lambda_estimate = self.inverted_mean(axe)
        alpha = self.PROBABILITY['alpha']

        # Посчитаем левую границу интервала
        a = 2 * n / (lambda_estimate * ss.chi2.ppf(1 - alpha / 2, 2 * n))

        # Правую
        b = 2 * n / (lambda_estimate * ss.chi2.ppf(alpha / 2, 2 * n))

        return [a, b]

    def limits_all(self):
        limits = np.array(((),))
        axis = self.predictions.shape[1]
        for axe in range(axis):
            limits = np.append(limits, self.limits(axe))

        return limits.reshape(axis, 2)

    def put_in_tube(self, estimate, limits):
        axis = self.predictions.shape[1]
        tubed = estimate
        for axe in range(axis):
            if (estimate[axe] < limits[axe][0]) or (estimate[axe] > limits[axe][1]):
                tubed[axe] = limits[axe][0]

        return tubed

    def tuber(self):
        limits = self.PROBABILITY['n'] * self.limits_all()
        for z in reversed(self.predictions):
            self.set_tubed_predictions(self.put_in_tube(z, limits))

        return self.tubed_predictions


def main():
    inv = Tubed_filter()

    for k in xrange(5000):
        inv.prediction()

    #inv.tuber()

    limits = inv.PROBABILITY['n'] * inv.limits_all()

    for graph in range(3):
        #plt.plot_triple(inv.predictions, inv.tubed_predictions, inv.ideals, graph)
        plt.plot_data_limited(inv.predictions, limits[graph], inv.ideals, graph)


if __name__ == '__main__':
    main()
