#!/usr/bin/env python
# -*- coding: utf-8 -*-

from matplotlib import pyplot as plt


def plot_data(data, extra_data=None, graph=0):
    a = [i for i in range(data.shape[0])]
    plt.plot(a, data.T[graph], 'r',)

    if extra_data is not None:
        plt.plot(a, extra_data.T[graph], 'b--',)

    plt.show()


def plot_data_limited(data, limits, extra_data=None, graph=0):
    a = [i for i in range(data.shape[0])]
    plt.plot(a, data.T[graph], 'r',
             a, [limits[0] for i in range(data.shape[0])], 'r--',
             a, [limits[1] for i in range(data.shape[0])], 'r--',)

    if extra_data is not None:
        plt.plot(a, extra_data.T[graph], 'b--',)

    plt.show()


def plot_triple(data, next_data, extra_data=None, graph=0):
    a = [i for i in range(data.shape[0])]
    plt.plot(a, data.T[graph], 'r',)
    plt.plot(a, next_data.T[graph], 'g',)

    if extra_data is not None:
        plt.plot(a, extra_data.T[graph], 'b--',)

    plt.show()


def plot_channel_data(plot_data, plot_raw_data, title=None):
    a = [i for i in range(plot_raw_data.shape[0])]
    fig = plt.figure(1)
    plt.subplot(311)
    if title:
        fig.canvas.set_window_title(title)

    plt.plot(a, plot_data.T[0], 'r',
             a, plot_raw_data.T[0], 'b--',)
    plt.subplot(312)
    plt.plot(a, plot_data.T[1], 'r',
             a, plot_raw_data.T[1], 'b--',)
    plt.subplot(313)
    plt.plot(a, plot_data.T[2], 'r',
             a, plot_raw_data.T[2], 'b--',)

    plt.show()


def plot_channel_triple(plot_data, plot_raw_data, plot_extra_data, title=None):
    a = [i for i in range(plot_raw_data.shape[0])]
    fig = plt.figure(1)
    plt.subplot(311)
    if title:
        fig.canvas.set_window_title(title)

    plt.plot(a, plot_data.T[0], 'r',
             a, plot_raw_data.T[0], 'b--',
             a, plot_extra_data.T[0], 'g--',)
    plt.subplot(312)
    plt.plot(a, plot_data.T[1], 'r',
             a, plot_raw_data.T[1], 'b--',
             a, plot_extra_data.T[1], 'g--',)
    plt.subplot(313)
    plt.plot(a, plot_data.T[2], 'r',
             a, plot_raw_data.T[2], 'b--',
             a, plot_extra_data.T[2], 'g--',)

    plt.show()


def main():
    pass


if __name__ == '__main__':
    main()
