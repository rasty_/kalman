#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

import plotter as plt


class ObjectModel(object):
    EARTH_CONSTS = {
        'g': 9.81,
        'R': 6371e3,
        '1/R': 1.56786504e-7
    }
    
    SENSE_CONSTS = {
        'betta': 4.848136811095e-9,  # рад в сек = 1e-3 град в час
        'T': 1,
    }

    NOIZE_CONSTS = {
        'loc': 0,
        'scale_V': 1e-2,  # 1e-4 .. 6,
        'scale_W': 1e-15,
    }

    INITIALS = {
        'dve': 0,
        'fn': 0,
        'wn': 0
    }

    def __init__(self, initials=None, noise=None):
        self.x = initials or np.array(self.INITIALS.values())  # x = np.array(dve, fn, wn)
        if noise:
            self.NOIZE_CONSTS['scale_V'] = noise['scale_V']
            self.NOIZE_CONSTS['scale_W'] = noise['scale_W']
            self.NOIZE_CONSTS['loc'] = noise['loc']
        self.z = self.x
        self.h = np.array((
            (1., 0., 0.),
            (0., 0., 0.),
            (0., 0., 0.),
        ))
        self.g = np.array((0., 0., 1.))
        self.a = np.array((
            (0., -self.EARTH_CONSTS['g'], 0.),
            (self.EARTH_CONSTS['1/R'], 0., 1.),
            (0., 0., -self.SENSE_CONSTS['betta'])
        ))
        self.f = np.identity(3) + self.a * self.SENSE_CONSTS['T']

        self.measures = np.array((self.z,))
        self.ideals = np.array((self.x,))

    def __next__(self):
        return self.measure()

    def __call__(self, *args, **kwargs):
        return self.__next__()

    def V(self):
        return np.array((np.random.normal(self.NOIZE_CONSTS['loc'], self.NOIZE_CONSTS['scale_V']), 0., 0.))

    def W(self):
        return np.array((0., 0., np.random.normal(self.NOIZE_CONSTS['loc'], self.NOIZE_CONSTS['scale_W'])))

    def x_update(self):
        self.x = np.dot(self.f, self.x) + self.W()   # x_k = np.array(dve_d, fn_d, wn_d)
        self.set_ideals(self.x)

        return self.x

    def z_update(self):
        self.z = np.dot(self.h, self.x) + self.V()

        return self.z

    def set_ideals(self, x):
        self.ideals = np.append(self.ideals, [x], axis=0)

    def set_measures(self, z):
        self.measures = np.append(self.measures, [z], axis=0)

    def measure(self):
        Z = self.z_update()
        self.x_update()
        self.set_measures(Z)

        return Z

    def get_measures(self):
        return self.measures


class ModelJumped(ObjectModel):
    def V(self):
        noise = np.random.normal(self.NOIZE_CONSTS['loc'], self.NOIZE_CONSTS['scale_V']) + self._get_jump()

        return np.array((noise, 0., 0.))

    def _get_jump(self, scale=10, chance=0.01):
        jump = 0.
        if np.random.random() < chance:
            jump = np.random.randn() * scale
        return jump


def main():
    model = ObjectModel()

    for k in xrange(5000):
        model.measure()

    for graph in range(3):
        plt.plot_data(model.measures, model.ideals, graph)




if __name__ == '__main__':
    main()
