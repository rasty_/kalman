#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

import plotter as plt
from filter import Kalman


class Inverted(Kalman):
    def __init__(self, *args, **kwargs):
        super(Inverted, self).__init__(*args, **kwargs)
        self.f_inv = np.linalg.inv(self.f)
        self.x_est = np.zeros(3)
        self.estimates = np.array(((),))

    def set_estimates(self, est):
        if self.estimates.size == 0:
            self.estimates = np.append(self.estimates, [est], axis=1)
            return

        self.estimates = np.insert(self.estimates, 0, est, axis=0)

    def x_estimates(self):
        X_k_k1 = np.dot(self.f_inv, self.x_est)
        P_k_k1 = self._dbldot(self.f_inv, self.p) + self._dbldot(self.g, self.Q())
        K_k = np.divide(np.dot(P_k_k1, self.h.T), (self._dbldot(self.h, P_k_k1)[0][0] + self.R()))

        self.p = np.dot((np.identity(3) - np.dot(K_k, self.h)), P_k_k1)
        self.x_est = X_k_k1 + np.dot(K_k, (self.z - np.dot(np.dot(self.h, self.f_inv), self.x_est)))

        return self.x_est

    def estimate(self):
        for z in reversed(self.measures):
            self.z = z
            X_est = self.x_estimates()
            self.set_estimates(X_est)

        return self.estimates


def main():
    inv = Inverted()

    for k in xrange(5000):
        inv()

    inv.estimate()

    for graph in range(3):
        plt.plot_triple(inv.predictions, inv.estimates, inv.ideals, graph)

    #plt.plot_channel_data(inv.predictions, inv.estimates

if __name__ == '__main__':
    main()
