#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

import plotter as plt
from filter import Kalman


class AdaptiveModel(Kalman):
    def __init__(self, *args, **kwargs):
        self.k = 0
        self.K_k = np.array((1, 0., 0.))
        self.c = 0.
        super(AdaptiveModel, self).__init__(*args, **kwargs)

    def x_prediction_update(self):
        self.k += 1.

        X_k_k1 = np.dot(self.f, self.x_pr)
        nu_k = self.z - np.dot(self.h, X_k_k1)
        k_1 = np.divide(1., self.k)
        self.c = ((1. - k_1) * self.c) + (k_1 * np.dot(nu_k, nu_k.T))

        P_k_k1 = self._dbldot(self.f, self.p) + self._dbldot(self.K_k, self.c)  # self._dbldot(self.g, self.Q())
        self.K_k = np.divide(np.dot(P_k_k1, self.h.T), (self._dbldot(self.h, P_k_k1)[0][0] + self.R()))

        self.x_pr = X_k_k1 + np.dot(self.K_k, nu_k)
        self.p = np.dot((np.identity(3) - np.dot(self.K_k, self.h)), P_k_k1)

        return self.x_pr


def main():
    kalm = AdaptiveModel()

    for k in xrange(5000):
        kalm.prediction()

    #for graph in range(3):
    #    plt.plot_data(kalm.predictions, kalm.ideals, graph)

    plt.plot_channel_data(kalm.predictions, kalm.ideals)

if __name__ == '__main__':
    main()
