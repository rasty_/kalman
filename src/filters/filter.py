#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

import plotter as plt
#from model import ModelJumped as Model
from model import ObjectModel as Model


class Kalman(Model):
    def __init__(self, *args, **kwargs):
        super(Kalman, self).__init__(*args, **kwargs)

        self.p = np.zeros((3, 3))
        self.x_pr = self.x
        self.predictions = np.array((self.x_pr,))

    def __next__(self):
        return self.prediction()

    def _dbldot(self, a, b):
        """
        F * P * F.T
        """
        return np.dot(np.dot(a, b), a.T)

    def R(self):
        V_k = self.V()

        return np.mean(np.dot(V_k, V_k.T))

    def Q(self):
        W_k = self.W()

        return np.mean(np.dot(W_k, W_k.T))

    def x_prediction_update(self):
        X_k_k1 = np.dot(self.f, self.x_pr)
        P_k_k1 = self._dbldot(self.f, self.p) + self._dbldot(self.g, self.Q())
        #K_k = np.dot(np.dot(P_k_k1, self.h.T), np.linalg.inv(self._dbldot(self.h, P_k_k1) + self.R()))
        K_k = np.divide(np.dot(P_k_k1, self.h.T), (self._dbldot(self.h, P_k_k1)[0][0] + self.R()))  # Dirty hack

        self.p = np.dot((np.identity(3) - np.dot(K_k, self.h)), P_k_k1)
        self.x_pr = X_k_k1 + np.dot(K_k, (self.z - np.dot(np.dot(self.h, self.f), self.x_pr)))

        return self.x_pr

    def set_predictions(self, x_pr):
        self.predictions = np.append(self.predictions, [x_pr], axis=0)

    def prediction(self):
        self.measure()
        X_pr = self.x_prediction_update()

        self.set_predictions(X_pr)

        return X_pr


class Kalman_Q(Kalman):
    def __init__(self, *args, **kwargs):
        super(Kalman_Q, self).__init__(*args, **kwargs)

        self.kalm_r = super(Kalman_Q, self).R()
        self.kalm_q = super(Kalman_Q, self).Q()

    def set_R(self, r):
        self.kalm_r = r

    def set_Q(self, q):
        self.kalm_q = q

    def R(self):
        return self.kalm_r

    def Q(self):
        return self.kalm_q


def main():
    kalm = Kalman()

    for k in xrange(5000):
        kalm.prediction()

    #for graph in range(3):
    #    plt.plot_data(kalm.predictions, kalm.ideals, graph)

    plt.plot_channel_data(kalm.predictions, kalm.ideals)


if __name__ == '__main__':
    main()
